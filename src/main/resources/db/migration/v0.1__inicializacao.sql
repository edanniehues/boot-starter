CREATE TABLE revisao (
  id bigserial,
  timestamp bigint,
  user_id bigint
);

ALTER TABLE revisao
  ADD CONSTRAINT revisao_id_pk PRIMARY KEY (id);
