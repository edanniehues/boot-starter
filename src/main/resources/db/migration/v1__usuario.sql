CREATE TABLE usuario (
  id             bigserial,
  criacao        timestamp with time zone NOT NULL,
  atualizacao    timestamp with time zone NOT NULL,
  email          varchar                  NOT NULL,
  senha          varchar                  NOT NULL,
  nome           varchar                  NOT NULL,
  ativo          boolean                  NOT NULL,
  perfil_usuario integer                  NOT NULL
);

ALTER TABLE usuario
  ADD CONSTRAINT pk_usuario_id PRIMARY KEY (id);

CREATE UNIQUE INDEX uk_usuario_email
  ON usuario (lower(email));


CREATE TABLE usuario_aud (
  id             bigint  NOT NULL,
  revisao        bigint  NOT NULL,
  tipo_revisao   smallint NOT NULL,
  criacao        timestamp with time zone,
  atualizacao    timestamp with time zone,
  email          varchar,
  senha          varchar,
  nome           varchar,
  ativo          boolean,
  perfil_usuario integer
);

ALTER TABLE usuario_aud
  ADD CONSTRAINT pk_usuario_aud_id_revisao PRIMARY KEY (id, revisao),
  ADD CONSTRAINT fk_usuario_aud_revisao FOREIGN KEY (revisao) REFERENCES revisao (id);

CREATE TABLE usuario_token (
  id          bigserial,
  criacao     timestamp with time zone NOT NULL,
  atualizacao timestamp with time zone NOT NULL,
  usuario_id  bigint                   NOT NULL,
  token       text                     NOT NULL
);

ALTER TABLE usuario_token
  ADD CONSTRAINT pk_usuario_token_id PRIMARY KEY (id),
  ADD CONSTRAINT uk_usuario_token_token UNIQUE (token),
  ADD CONSTRAINT fk_usuario_token_usuario_id FOREIGN KEY (usuario_id) REFERENCES usuario (id);

CREATE TABLE usuario_token_aud (
  id           bigserial,
  revisao      bigint  NOT NULL,
  tipo_revisao smallint NOT NULL,
  criacao      timestamp with time zone,
  atualizacao  timestamp with time zone,
  usuario_id   bigint,
  token        text
);

ALTER TABLE usuario_token_aud
  ADD CONSTRAINT pk_usuario_token_aud_id_revisao PRIMARY KEY (id, revisao),
  ADD CONSTRAINT fk_usuario_token_aud_revisao FOREIGN KEY (revisao) REFERENCES revisao (id);