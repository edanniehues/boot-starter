import { Injectable, NgModule } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterModule,
  RouterStateSnapshot,
  Routes
} from '@angular/router';
import { LoginViewComponent } from './login/views/login-view.component';
import { HomeViewComponent } from './home/home-view.component';

export const JWT_TOKEN = 'JWT_TOKEN';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(!localStorage.getItem(JWT_TOKEN)) {
      this.router.navigate(['login']);
    }
    return true;
  }
}

const routes: Routes = [
  {
    path: 'login',
    component: LoginViewComponent
    /* BUNDLE */
  },
  {
    path: '',
    component: HomeViewComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthGuardService],
  exports: [RouterModule]
})
export class AppRoutingModule { }
