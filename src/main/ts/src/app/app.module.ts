import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatButtonModule, MatCardModule, MatInputModule, MatProgressBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CovalentCommonModule, CovalentLayoutModule } from '@covalent/core';
import { HomeViewComponent } from './home/home-view.component';
import { LoginViewComponent } from './login/views/login-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from './services/authentication.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ApiInterceptor } from './services/interceptors/api-interceptor';
import { LoadingInterceptor } from './services/interceptors/loading-interceptor';
import { LoadingService } from './services/loading.service';
import { UsuarioService } from './services/usuario.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginViewComponent,
    HomeViewComponent
  ],
  imports: [
    // Angular
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // Material
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatProgressBarModule,
    // Covalent
    CovalentCommonModule,
    CovalentLayoutModule,
    // App
    AppRoutingModule,
  ],
  providers: [
    // Core
    AuthenticationService,
    LoadingService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    },
    // Domain
    UsuarioService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
