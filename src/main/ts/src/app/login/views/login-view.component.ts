import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

interface LoginModel {
  email: string,
  senha: string
}

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {

  @ViewChild('loginForm') public loginForm: NgForm;

  public model: LoginModel = {
    email: '',
    senha: ''
  };


  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
    if(this.authenticationService.isLoggedIn()) {
      this.router.navigate(['']);
    }
  }

  public login() {
    if(this.loginForm.valid) {
      this.authenticationService.createToken(this.model.email, this.model.senha)
        .then(() => this.router.navigate(['']))
        .catch(error => alert(error));
    } else {
      alert('Inválido!');
    }
  }

}
