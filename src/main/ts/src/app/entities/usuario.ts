export type PerfilUsuario = 'PROFESSOR' | 'ADMINISTRADOR';

export interface Usuario {
  id?: number,
  email?: string,
  nome?: string,
  ativo?: boolean,
  senha?: string,
  perfilUsuario?: PerfilUsuario
}
