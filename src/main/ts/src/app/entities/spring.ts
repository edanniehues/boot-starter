import { HttpParams } from '@angular/common/http';

export interface Sort {
  orders: SortOrder[]
}

export interface SortOrder {
  direction: SortDirection,
  property: string,
  nullHandlingHint?: NullHandling
}

export type SortDirection = 'ASC' | 'DESC';
export type NullHandling = 'NATIVE' | 'NULLS_FIRST' | 'NULLS_LAST';

export interface Pageable {
  page: number,
  size: number,
  sort?: Sort
}

export interface Page<T> {
  content: T[],
  totalElements: number,
  numberOfElements: number,
  totalPages: number,
  pageable?: Pageable,
  last: boolean,
  first: boolean
}

export class PageRequest {
  constructor(private page: number, private size: number, private sort: string[] = []) {

  }

  public toParams(existing: HttpParams): HttpParams {
    existing = existing.set('page', this.page.toString());
    existing = existing.set('size', this.size.toString());
    this.sort.forEach(sort => {
      existing = existing.append('sort', sort);
    });

    return existing;
  }
}
