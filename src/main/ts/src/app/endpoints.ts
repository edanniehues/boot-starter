export const createTokenPath = () => '/create_token';
export const deleteTokenPath = () => '/delete_token';

export const usuarioPath = id => `/api/usuarios/${id}`;
export const usuariosPath = () => '/api/usuarios';
