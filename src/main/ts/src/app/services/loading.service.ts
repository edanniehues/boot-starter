import { Injectable } from '@angular/core';
import 'rxjs/add/observable/from';

export type EventType = 'start' | 'stop';
export type LoadingCallback = (type: EventType) => any;

@Injectable()
export class LoadingService {
  private requests = 0;
  private listeners: LoadingCallback[] = [];

  public incrementRequests(): void {
    this.requests++;
  }

  public decrementRequests(): void {
    this.requests--;
  }

  public listen(callback: LoadingCallback) {
    this.listeners.push(callback);
  }

  private checkIfChanged() {
    const type: EventType = this.requests > 0 ? 'start' : 'stop';
    this.listeners.forEach(listener => listener(type));
  }
}
