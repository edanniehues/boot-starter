import { JWT_TOKEN } from '../app-routing.module';

export default function apiFetch(info: RequestInfo, init?: RequestInit): Promise<Response> {
  const token = localStorage.getItem(JWT_TOKEN);
  const ownInit: RequestInit = init ? init : {};
  if(!ownInit.headers) {
    ownInit.headers = {}
  }
  ownInit.headers['Authorization'] = `Bearer ${token}`;

  return fetch(info, ownInit)
    .then(response => response.status === 403 ? Promise.reject('Token expirado.') : Promise.resolve(response));
};
