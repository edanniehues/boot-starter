import { Injectable } from '@angular/core';
import { Usuario } from '../entities/usuario';
import { createTokenPath, deleteTokenPath } from '../endpoints';
import { JWT_TOKEN } from '../app-routing.module';
import apiFetch from './api-fetch';

@Injectable()
export class AuthenticationService {
  private usuario: Usuario = null;

  public createToken(email: string, senha: string): Promise<Usuario> {
    return fetch(
      createTokenPath(),
      {
        method: 'POST',
        body: JSON.stringify({ email, senha })
      })
      .then(response => response.json())
      .then(json => {
        if(json['token']) {
          const {token, usuario} = json;
          localStorage.setItem(JWT_TOKEN, token);
          this.usuario = usuario;
          return Promise.resolve(usuario as Usuario);
        } else {
          return Promise.reject(json['message']);
        }
      });
  }

  public deleteToken(): Promise<void> {
    if(!this.isLoggedIn()) {
      return Promise.reject('O usuário não está autenticado.');
    }
    return apiFetch(deleteTokenPath())
      .then(response => {
        if (response.ok) {
          localStorage.removeItem(JWT_TOKEN);
          return Promise.resolve();
        } else {
          return Promise.reject('Não foi possível excluir o token do usuário.');
        }
      });
  }

  public isLoggedIn(): boolean {
    return !!localStorage.getItem(JWT_TOKEN);
  }

  public getCurrentUser(): Usuario {
    return this.usuario;
  }
}
