import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JWT_TOKEN } from '../../app-routing.module';
import { Injectable } from '@angular/core';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedReq = req.clone({ headers: req.headers.append('Authorization', `Bearer ${localStorage.getItem(JWT_TOKEN)}`) });
    return next.handle(clonedReq);
  }
}
