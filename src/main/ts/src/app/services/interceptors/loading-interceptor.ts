import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { LoadingService } from '../loading.service';
import 'rxjs/add/operator/do';
import { Injectable } from '@angular/core';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  constructor(private loadingService: LoadingService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingService.incrementRequests();
    return next.handle(req).do(event => {
      if(event instanceof HttpResponse) {
        this.loadingService.decrementRequests();
      }
    });
  }

}
