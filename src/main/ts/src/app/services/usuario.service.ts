import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Usuario } from '../entities/usuario';
import { Observable } from 'rxjs/Observable';
import { usuarioPath, usuariosPath } from '../endpoints';
import { Page, PageRequest } from '../entities/spring';

@Injectable()
export class UsuarioService {
  constructor(private http: HttpClient) { }

  public findById(id: number): Observable<Usuario> {
    return this.http.get<Usuario>(usuarioPath(id));
  }

  public listByFilters(filters: string, pageRequest: PageRequest): Observable<Page<Usuario>> {
    const params = new HttpParams().set('filters', filters);
    return this.http.get<Page<Usuario>>(usuariosPath(), {
      params: pageRequest.toParams(params)
    });
  }
}
