import { Component, OnInit } from '@angular/core';
import { Usuario } from '../entities/usuario';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { Page, PageRequest } from '../entities/spring';
import { LoadingService } from '../services/loading.service';

interface Model {
  userId?: string,
  usuario?: Usuario,
  loading: boolean,
  page?: Page<Usuario>
}

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.scss']
})
export class HomeViewComponent implements OnInit {

  model: Model = {
    loading: false
  };

  constructor(
    private authenticationService: AuthenticationService,
    private loadingService: LoadingService,
    private router: Router,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.loadingService.listen(type => this.model.loading = type === 'start');
  }

  findUser() {
    const id = +this.model.userId;
    this.usuarioService.findById(id)
      .subscribe(usuario => this.model.usuario = usuario,
        err => console.log(err));
  }

  listUsers() {
    this.usuarioService.listByFilters(this.model.userId, new PageRequest(0, 15, ['nome,ASC']))
      .subscribe(page => this.model.page = page);
  }

  logout() {
    this.authenticationService.deleteToken().then(() => this.router.navigate(['login']));
  }

}
