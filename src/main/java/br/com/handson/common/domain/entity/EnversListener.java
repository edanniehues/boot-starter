package br.com.handson.common.domain.entity;

import java.io.Serializable;

import org.hibernate.envers.EntityTrackingRevisionListener;
import org.hibernate.envers.RevisionType;

public class EnversListener implements EntityTrackingRevisionListener
{
	@Override
	public void entityChanged( Class aClass, String s, Serializable serializable, RevisionType revisionType, Object o )
	{

	}

	@Override
	public void newRevision( Object revision )
	{

	}
}
