package br.com.handson.common.domain.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@MappedSuperclass
public abstract class AbstractEntity implements Serializable
{
	/**
	 *
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	/**
	 *
	 */
	@Column(nullable = false, updatable = false)
	protected LocalDateTime criacao;

	/**
	 *
	 */
	@Column(nullable = false)
	protected LocalDateTime atualizacao;

	@PrePersist
	protected void refreshCreated()
	{
		LocalDateTime momentoAtual = LocalDateTime.now();
		this.criacao = momentoAtual;
		this.atualizacao = momentoAtual;
	}

	@PreUpdate
	protected void refreshUpdated()
	{
		this.atualizacao = LocalDateTime.now();
	}
}
