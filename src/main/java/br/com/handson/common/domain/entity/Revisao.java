package br.com.handson.common.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

@Entity
@Data
@EqualsAndHashCode
@NoArgsConstructor
@RevisionEntity(EnversListener.class)
public class Revisao implements Serializable
{
	/**
	 *
	 */
	@RevisionNumber
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/**
	 *
	 */
	@RevisionTimestamp
	private long timestamp;

	/**
	 *
	 */
	private Long userId;
}
