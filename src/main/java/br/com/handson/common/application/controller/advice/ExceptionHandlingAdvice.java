package br.com.handson.common.application.controller.advice;

import br.com.handson.common.application.dto.ConvertedException;
import br.com.handson.common.application.dto.ErrorMessage;
import br.com.handson.common.infrastructure.validation.InvalidInputException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandlingAdvice
{
	@ExceptionHandler
	ResponseEntity<ErrorMessage> handleOtherExceptions( Throwable exception )
	{
		return new ResponseEntity<>(
				ErrorMessage.of( exception.getMessage(), ConvertedException.of( exception ) ),
				exception instanceof InvalidInputException ? HttpStatus.BAD_REQUEST : HttpStatus.INTERNAL_SERVER_ERROR );
	}
}
