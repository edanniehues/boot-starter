package br.com.handson.common.application.configuration;

import java.util.Locale;

import br.com.handson.common.application.i18n.CustomResourceBundleMessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LocaleConfiguration
{
	@Bean
	public CustomResourceBundleMessageSource messageSource()
	{
		Locale.setDefault( new Locale( "pt", "BR" ) );

		final CustomResourceBundleMessageSource messageSource = new CustomResourceBundleMessageSource();
		messageSource.setAlwaysUseMessageFormat( true );
		messageSource.setDefaultEncoding( "UTF-8" );
		messageSource.setBasenames(
				"classpath:i18n/labels",
				"classpath:i18n/messages"
		);

		return messageSource;
	}
}
