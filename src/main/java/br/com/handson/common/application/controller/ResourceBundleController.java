package br.com.handson.common.application.controller;

import java.util.Locale;
import java.util.Properties;

import br.com.handson.common.application.i18n.CustomResourceBundleMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceBundleController
{
	@Autowired
	private CustomResourceBundleMessageSource messageSource;

	@GetMapping("/bundles")
	public Properties getBundle( Locale locale )
	{
		return messageSource.getProperties( locale );
	}
}
