package br.com.handson.common.application.dto;

import br.com.handson.starter.domain.entity.usuario.Usuario;
import lombok.Data;

@Data(staticConstructor = "of")
public class LoginResponse
{
	private final String token;

	private final Usuario usuario;
}
