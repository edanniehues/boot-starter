package br.com.handson.common.application.dto;

import lombok.Data;

@Data(staticConstructor = "of")
public class LoginCredentials
{
	private final String email;

	private final String senha;
}
