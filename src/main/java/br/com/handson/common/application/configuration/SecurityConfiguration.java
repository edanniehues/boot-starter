package br.com.handson.common.application.configuration;

import br.com.handson.common.infrastructure.security.jwt.JwtAuthenticationFilter;
import br.com.handson.common.infrastructure.security.jwt.JwtAuthenticationService;
import br.com.handson.common.infrastructure.security.jwt.JwtLoginFilter;
import br.com.handson.common.infrastructure.security.jwt.JwtLogoutHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
	@Autowired
	private JwtAuthenticationService jwtAuthenticationService;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private ObjectMapper objectMapper;

	@Bean
	public PasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure( AuthenticationManagerBuilder auth ) throws Exception
	{
		auth
				.userDetailsService( userDetailsService )
				.passwordEncoder( passwordEncoder() );
	}

	@Override
	public void configure( WebSecurity web ) throws Exception
	{
		super.configure( web );
	}

	@Override
	protected void configure( HttpSecurity http ) throws Exception
	{
		http
				.authorizeRequests()
				.antMatchers( "/*" ).permitAll()
				.antMatchers( "/api/**" )
				.authenticated()
				.and()
				.csrf().disable()
				.cors().disable()
				.formLogin().disable()
				.addFilterBefore( new JwtAuthenticationFilter( jwtAuthenticationService ), UsernamePasswordAuthenticationFilter.class )
				.addFilterBefore( new JwtLoginFilter( "/create_token", authenticationManager(), jwtAuthenticationService, objectMapper ), UsernamePasswordAuthenticationFilter.class )
				.logout().logoutUrl( "/delete_token" )
				.addLogoutHandler( new JwtLogoutHandler( jwtAuthenticationService ) ).logoutSuccessHandler( new HttpStatusReturningLogoutSuccessHandler( HttpStatus.OK ) );

	}
}
