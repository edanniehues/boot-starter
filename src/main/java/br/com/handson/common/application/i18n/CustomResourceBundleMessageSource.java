package br.com.handson.common.application.i18n;

import java.util.Locale;
import java.util.Properties;

import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public class CustomResourceBundleMessageSource extends ReloadableResourceBundleMessageSource
{
	public CustomResourceBundleMessageSource()
	{
		TranslationService.setMessageSource( this );
	}

	/**
	 *
	 * @param locale Locale para qual retornar todos os itens
	 * @return Todos os itens de uma locale
	 */
	public Properties getProperties( Locale locale )
	{
		super.clearCacheIncludingAncestors();
		return getMergedProperties( locale ).getProperties();
	}
}
