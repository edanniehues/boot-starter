package br.com.handson.common.application.dto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

@Data(staticConstructor = "of")
public class ErrorMessage
{
	private final String message;

	@JsonTypeInfo(property = "type", use = JsonTypeInfo.Id.CLASS)
	private final ConvertedException exception;
}
