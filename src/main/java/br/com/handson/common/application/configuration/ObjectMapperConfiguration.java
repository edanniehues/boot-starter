package br.com.handson.common.application.configuration;

import br.com.handson.common.infrastructure.jackson.mixins.OrderMixin;
import br.com.handson.common.infrastructure.jackson.mixins.PageImplMixin;
import br.com.handson.common.infrastructure.jackson.mixins.PageMixin;
import br.com.handson.common.infrastructure.jackson.mixins.PageRequestMixin;
import br.com.handson.common.infrastructure.jackson.mixins.PageableMixin;
import br.com.handson.common.infrastructure.jackson.mixins.SortMixin;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
public class ObjectMapperConfiguration implements Jackson2ObjectMapperBuilderCustomizer
{
	@Override
	public void customize( Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder )
	{
		jacksonObjectMapperBuilder
				.mixIn( Page.class, PageMixin.class )
				.mixIn( PageImpl.class, PageImplMixin.class )
				.mixIn( Pageable.class, PageableMixin.class )
				.mixIn( PageRequest.class, PageRequestMixin.class )
				.mixIn( Sort.class, SortMixin.class )
				.mixIn( Sort.Order.class, OrderMixin.class )
				.featuresToDisable(
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
						SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
				)
				.defaultTyping(
						new ObjectMapper.DefaultTypeResolverBuilder( ObjectMapper.DefaultTyping.JAVA_LANG_OBJECT )
								.init( JsonTypeInfo.Id.CLASS, null )
								.inclusion( JsonTypeInfo.As.PROPERTY )
								.typeProperty( "@type" )
				)
				.modulesToInstall(
						new Jdk8Module(),
						new JavaTimeModule(),
						new Hibernate5Module()
				);
	}
}
