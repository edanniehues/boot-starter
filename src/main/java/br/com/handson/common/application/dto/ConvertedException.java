package br.com.handson.common.application.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "cause")
public class ConvertedException
{
	private final String type;

	private final String message;

	private final ConvertedException cause;

	private final StackTraceElement[] stackTrace;

	public static ConvertedException of( Throwable throwable )
	{
		return new ConvertedException(
				throwable.getClass().getName(),
				throwable.getMessage(),
				throwable.getCause() == null || throwable.getCause() == throwable ? null : ConvertedException.of( throwable.getCause() ),
				throwable.getStackTrace() );

	}
}
