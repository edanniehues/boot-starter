package br.com.handson.common.application.configuration;

import java.net.URI;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration
{
	@Bean
	public DataSource dataSource(
			@Value("${spring.datasource.url}") String url,
			@Value("${spring.datasource.username}") String username,
			@Value("${spring.datasource.password}") String password,
			@Value("${application.datasource.bootstrap}") String bootstrapURI )
	{
		HikariDataSource dataSource = new HikariDataSource();
		dataSource.setJdbcUrl( url );
		dataSource.setUsername( username );
		dataSource.setPassword( password );

		if ( bootstrapURI != null )
		{
			bootstrapDataSource( dataSource, bootstrapURI );
		}
		return dataSource;
	}

	private void bootstrapDataSource( DataSource dataSource, String bootstrapURI )
	{
		String script = new Scanner( getClass().getResourceAsStream( bootstrapURI ), "UTF-8" ).useDelimiter( "\\A" ).next();
		try (
				Connection connection = dataSource.getConnection();
				CallableStatement statement = connection.prepareCall( script );
		)
		{
			statement.executeUpdate();
		}
		catch ( SQLException e )
		{
			throw new RuntimeException( "Não foi possível inicializar a DataSource.", e );
		}
	}
}
