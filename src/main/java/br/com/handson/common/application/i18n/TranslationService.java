package br.com.handson.common.application.i18n;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

public class TranslationService
{
	private static MessageSource messageSource;

	public static String translate( String key, Object... params )
	{
		return messageSource.getMessage( key, params, LocaleContextHolder.getLocale() );
	}

	public static void setMessageSource( MessageSource messageSource )
	{
		TranslationService.messageSource = messageSource;
	}
}
