package br.com.handson.common.application.request;

import java.util.Optional;

import br.com.handson.starter.domain.entity.usuario.Usuario;
import org.springframework.security.core.context.SecurityContextHolder;

public class RequestContext
{
	public static Optional<Usuario> getUsuario()
	{
		return
				Optional.ofNullable( SecurityContextHolder.getContext().getAuthentication() )
						.map( authentication -> (Usuario) authentication.getPrincipal() );
	}
}
