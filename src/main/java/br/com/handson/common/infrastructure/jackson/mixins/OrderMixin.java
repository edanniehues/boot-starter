package br.com.handson.common.infrastructure.jackson.mixins;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.NullHandling;

public abstract class OrderMixin
{
	/**
	 * 
	 */
	@JsonCreator
	public OrderMixin( @JsonProperty("direction") Direction direction, @JsonProperty("property") String property, @JsonProperty("nullHandlingHint") NullHandling nullHandlingHint )
	{
	}
}