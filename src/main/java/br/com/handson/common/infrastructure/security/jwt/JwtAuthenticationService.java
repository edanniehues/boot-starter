package br.com.handson.common.infrastructure.security.jwt;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import br.com.handson.starter.domain.entity.usuario.UsuarioToken;
import br.com.handson.starter.domain.repository.usuario.IUsuarioRepository;
import br.com.handson.starter.domain.repository.usuario.IUsuarioTokenRepository;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * Responsável por retornar ao cliente o novo token após logar, e por validar que os tokens enviados estejam ativos e que
 * não foram removidos.
 */
@Component
public class JwtAuthenticationService
{
	private static final String AUTHORIZATION_HEADER = "Authorization";

	private static final String TOKEN_PREFIX = "Bearer";

	/**
	 *
	 */
	private final IUsuarioRepository usuarioRepository;

	/**
	 *
	 */
	private final IUsuarioTokenRepository usuarioTokenRepository;

	/**
	 *
	 */
	private final Algorithm algorithm;

	@Autowired
	public JwtAuthenticationService(
			IUsuarioRepository usuarioRepository,
			IUsuarioTokenRepository usuarioTokenRepository,
			@Value("${application.jwt-secret}") String jwtSecret
	) throws UnsupportedEncodingException
	{
		this.usuarioRepository = usuarioRepository;
		this.usuarioTokenRepository = usuarioTokenRepository;

		this.algorithm = Algorithm.HMAC512( jwtSecret == null ? UUID.randomUUID().toString() : jwtSecret );
	}

	@Transactional
	public String addAuthentication( HttpServletResponse response, String email )
	{
		String jwt =
				JWT.create()
						.withSubject( email )
						.withExpiresAt( Date.from( LocalDateTime.now().plusMonths( 6 ).toInstant( ZoneOffset.UTC ) ) )
						.sign( algorithm );

		UsuarioToken usuarioToken = new UsuarioToken();
		usuarioToken.setToken( jwt );
		usuarioToken.setUsuario( usuarioRepository.findByEmail( email ).orElse( null ) );
		usuarioTokenRepository.saveAndFlush( usuarioToken );

		return jwt;
	}

	public Authentication getAuthentication( HttpServletRequest request )
	{
		try
		{
			return Optional.ofNullable( request.getHeader( AUTHORIZATION_HEADER ) )
					.map( header -> header.replace( TOKEN_PREFIX + " ", "" ) )
					.map( token -> JWT.require( algorithm ).build().verify( token ) )
					.map( DecodedJWT::getToken )
					.flatMap( this.usuarioRepository::findByToken )
					.map( usuario -> new UsernamePasswordAuthenticationToken( usuario, null, usuario.getAuthorities() ) )
					.orElse( null );
		}
		catch ( Exception e )
		{
			return null;
		}
	}

	@Transactional
	public void dropAuthentication( HttpServletRequest request )
	{
		String currentToken = request.getHeader( AUTHORIZATION_HEADER ).replace( TOKEN_PREFIX + " ", "" );
		UsuarioToken usuarioToken = usuarioTokenRepository.findByToken( currentToken ).get();
		usuarioTokenRepository.delete( usuarioToken );
	}
}
