package br.com.handson.common.infrastructure.security.jwt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

public class JwtLogoutHandler implements LogoutHandler
{
	private final JwtAuthenticationService jwtAuthenticationService;

	public JwtLogoutHandler( JwtAuthenticationService jwtAuthenticationService )
	{
		this.jwtAuthenticationService = jwtAuthenticationService;
	}

	@Override
	public void logout( HttpServletRequest request, HttpServletResponse response, Authentication authentication )
	{
		if ( authentication != null )
		{
			jwtAuthenticationService.dropAuthentication( request );
		}
	}
}
