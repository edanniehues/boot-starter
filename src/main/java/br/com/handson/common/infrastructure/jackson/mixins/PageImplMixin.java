package br.com.handson.common.infrastructure.jackson.mixins;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Pageable;

public abstract class PageImplMixin
{
	/**
	 * 
	 */
	@JsonCreator
	public PageImplMixin( @JsonProperty("content") List<?> content, @JsonProperty(value="pageable") Pageable pageable, @JsonProperty("totalElements") long total )
	{
	}
}