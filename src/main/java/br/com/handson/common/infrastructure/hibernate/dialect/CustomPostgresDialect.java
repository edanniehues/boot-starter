package br.com.handson.common.infrastructure.hibernate.dialect;

import br.com.handson.common.infrastructure.hibernate.functions.FilterFunction;
import org.hibernate.dialect.PostgreSQL95Dialect;

/**
 * Dialeto customizado do Postgres para inclusão de funções.
 */
public class CustomPostgresDialect extends PostgreSQL95Dialect
{
	public CustomPostgresDialect()
	{
		super();
		registerFunction( "filter", new FilterFunction() );
	}
}
