package br.com.handson.common.infrastructure.authentication;

import br.com.handson.starter.domain.repository.usuario.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService
{
	private final IUsuarioRepository usuarioRepository;

	@Autowired
	public UserDetailsService( IUsuarioRepository usuarioRepository )
	{
		this.usuarioRepository = usuarioRepository;
	}

	@Override
	public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException
	{
		return usuarioRepository.findByEmail( username ).orElseThrow( () -> new UsernameNotFoundException( username ) );
	}
}
