package br.com.handson.common.infrastructure.jackson.mixins;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.data.domain.PageImpl;

@JsonTypeInfo(
	use = JsonTypeInfo.Id.NAME,
	include = JsonTypeInfo.As.PROPERTY,
	property = "@type"
)
@JsonSubTypes({
	@Type(value = PageImpl.class, name = "PageImpl")
})
public abstract class PageMixin
{
}