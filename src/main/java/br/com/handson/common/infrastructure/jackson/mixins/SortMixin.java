package br.com.handson.common.infrastructure.jackson.mixins;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.domain.Sort.Order;

public abstract class SortMixin
{
	/**
	 * 
	 */
	@JsonCreator
	public SortMixin( @JsonProperty("orders") List<Order> orders )
	{
	}
}