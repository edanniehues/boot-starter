package br.com.handson.common.infrastructure.jackson.mixins;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.springframework.data.domain.PageRequest;

@JsonTypeInfo(
	use = JsonTypeInfo.Id.NAME,
	include = JsonTypeInfo.As.PROPERTY,
	property = "@type"
)
@JsonSubTypes({
	@Type(value = PageRequest.class, name = "PageRequest")
})
public abstract class PageableMixin
{
}