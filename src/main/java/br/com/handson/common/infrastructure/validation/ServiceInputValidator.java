package br.com.handson.common.infrastructure.validation;

public class ServiceInputValidator
{
	public static void validate( boolean expression, String message )
	{
		if ( !expression )
		{
			throw new InvalidInputException( message );
		}
	}
}
