package br.com.handson.common.infrastructure.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

public abstract class AbstractMailRepository
{
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${application.external-url}")
	private String externalUrl;

	/**
	 *
	 * @return contexto do Thymeleaf já com parâmetros pré-definidos
	 */
	protected Context getDefaultContext()
	{
		final Context context = new Context();
		context.setVariable( "externalUrl", externalUrl );
		return context;
	}

	/**
	 *
	 * @param message mimeMessage
	 * @return messageHelper já com parâmetros pré-definidos
	 * @throws MessagingException
	 */
	protected MimeMessageHelper getDefaultMessage( MimeMessage message ) throws MessagingException
	{
		return new MimeMessageHelper( message, true, "UTF-8" );
	}

	/**
	 * Renderiza o template da mensagem de email.
	 *
	 * @param messageHelper mensagem
	 * @param context contexto do thymeleaf
	 * @throws MessagingException
	 */
	protected void renderMessage( MimeMessageHelper messageHelper, Context context ) throws MessagingException
	{
		final String content = templateEngine.process( "mail/template", context );
		messageHelper.setText( content, true );
	}
}
