package br.com.handson.common.infrastructure;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import br.com.handson.common.application.i18n.CustomResourceBundleMessageSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class JasperReportManager
{
	@Autowired
	private DataSource dataSource;

	@Autowired
	private CustomResourceBundleMessageSource messageSource;

	public ByteArrayOutputStream exportToPDF( Map<String, Object> parameters, String report )
	{
		try
		{
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream( fillReport( loadReport( report ), parameters ), outputStream );
			return outputStream;
		}
		catch ( Exception e )
		{
			throw new IllegalStateException( "Não foi possível exportar o relatório para PDF: " + report, e );
		}
	}

	public ByteArrayOutputStream exportToXLS( Map<String, Object> parameters, String report )
	{
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet( true );
		configuration.setDetectCellType( true );
		configuration.setCollapseRowSpan( true );
		configuration.setWhitePageBackground( true );
		return exportToXLS( parameters, report, configuration );
	}

	public ByteArrayOutputStream exportToXLS( Map<String, Object> parameters, String report, SimpleXlsReportConfiguration configuration )
	{
		try
		{
			final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			final JRXlsExporter xlsExporter = new JRXlsExporter();

			xlsExporter.setExporterInput( new SimpleExporterInput( fillReport( loadReport( report ), parameters ) ) );
			xlsExporter.setExporterOutput( new SimpleOutputStreamExporterOutput( outputStream ) );
			return outputStream;
		}
		catch ( Exception e )
		{
			throw new IllegalStateException( "Não foi possível exportar o relatório para PDF: " + report, e );
		}
	}

	public Map<String,Object> getDefaultParameters()
	{
		Map<String, Object> map = new HashMap<>();
		map.put( JRParameter.REPORT_RESOURCE_BUNDLE, new MessageSourceResourceBundle( messageSource, LocaleContextHolder.getLocale() ) );
		return map;
	}

	private JasperReport loadReport( String path )
	{
		InputStream stream = getClass().getResourceAsStream( path );
		Assert.notNull( stream, "O caminho para o relatório não existe: " + path );
		try
		{
			return (JasperReport) JRLoader.loadObject( stream );
		}
		catch ( JRException e )
		{
			throw new IllegalArgumentException( "Não foi possível carregar o relatório compilado " + path, e );
		}
	}

	private JasperPrint fillReport( JasperReport jasperReport, Map<String, Object> parameters )
	{
		try ( Connection connection = dataSource.getConnection() )
		{
			return JasperFillManager.fillReport( jasperReport, parameters, connection );
		}
		catch ( Exception e )
		{
			throw new IllegalStateException( "Não foi possível preencher o relatório.", e );
		}
	}
}
