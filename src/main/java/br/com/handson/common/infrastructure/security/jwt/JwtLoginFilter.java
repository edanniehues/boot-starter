package br.com.handson.common.infrastructure.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.handson.common.application.dto.LoginCredentials;
import br.com.handson.common.application.dto.LoginResponse;
import br.com.handson.starter.domain.entity.usuario.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter
{
	private final JwtAuthenticationService jwtAuthenticationService;
	private final ObjectMapper objectMapper;

	public JwtLoginFilter( String url, AuthenticationManager authenticationManager, JwtAuthenticationService jwtAuthenticationService, ObjectMapper objectMapper )
	{
		super( new AntPathRequestMatcher( url, "POST" ) );
		this.jwtAuthenticationService = jwtAuthenticationService;
		this.objectMapper = objectMapper;
		setAuthenticationManager( authenticationManager );
	}

	@Override
	public Authentication attemptAuthentication( HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException, IOException, ServletException
	{
		LoginCredentials credentials = new ObjectMapper().readValue( request.getInputStream(), LoginCredentials.class );
		return getAuthenticationManager().authenticate(
				new UsernamePasswordAuthenticationToken(
						credentials.getEmail(),
						credentials.getSenha()
				)
		);
	}

	@Override
	protected void successfulAuthentication( HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult ) throws IOException, ServletException
	{
		String token = jwtAuthenticationService.addAuthentication( response, authResult.getName() );
		Usuario usuario = (Usuario) authResult.getPrincipal();

		response.setContentType( "application/json" );
		response.getOutputStream().write( objectMapper.writeValueAsBytes( LoginResponse.of( token, usuario ) ) );
	}
}
