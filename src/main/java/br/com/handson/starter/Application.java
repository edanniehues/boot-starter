package br.com.handson.starter;

import br.com.handson.common.CommonConfiguration;
import br.com.handson.common.domain.entity.AbstractEntity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackageClasses = {
		CommonConfiguration.class,
		Application.class
})
@EntityScan(basePackageClasses = {Application.class, AbstractEntity.class})
public class Application
{
	public static void main( String[] args )
	{
		SpringApplication.run( Application.class, args );
	}
}
