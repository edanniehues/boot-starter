package br.com.handson.starter.domain.entity.usuario;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;

public enum PerfilUsuario implements GrantedAuthority
{
	ADMINISTRADOR,     // 0
	PROFESSOR          // 1
	;

	@Override
	public String getAuthority()
	{
		return name();
	}

	public Set<GrantedAuthority> getAuthorities()
	{
		Set<GrantedAuthority> authorities = new HashSet<>();
		authorities.add( this );

		if ( this.equals( ADMINISTRADOR ) )
		{
			authorities.add( PROFESSOR );
		}

		return authorities;
	}
}
