package br.com.handson.starter.domain.service;

import static br.com.handson.common.application.i18n.TranslationService.translate;

import br.com.handson.common.infrastructure.validation.InvalidInputException;
import br.com.handson.starter.domain.entity.usuario.Usuario;
import br.com.handson.starter.domain.repository.usuario.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UsuarioService
{
	@Autowired
	private IUsuarioRepository usuarioRepository;

	public Usuario findById( long id )
	{
		return this.usuarioRepository.findById( id ).orElseThrow( () -> new InvalidInputException( translate( "usuario.error.notFound" ) ) );
	}

	public Page<Usuario> listByFilters( String filters, Pageable pageable )
	{
		return this.usuarioRepository.listByFilters( filters, pageable );
	}

}
