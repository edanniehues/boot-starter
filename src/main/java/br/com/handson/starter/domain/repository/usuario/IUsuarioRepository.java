package br.com.handson.starter.domain.repository.usuario;

import java.util.Optional;

import br.com.handson.starter.domain.entity.usuario.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long>
{
	@Query("SELECT usuario FROM UsuarioToken WHERE token = :token")
	Optional<Usuario> findByToken( @Param("token") String token );

	Optional<Usuario> findByEmail( String email );

	@Query("FROM Usuario usuario WHERE filter(:filters, usuario.nome, usuario.email) = true")
	Page<Usuario> listByFilters( @Param( "filters" ) String filters, Pageable pageable );
}
