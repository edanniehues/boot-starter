package br.com.handson.starter.domain.repository.usuario;

import java.util.Optional;

import br.com.handson.starter.domain.entity.usuario.UsuarioToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuarioTokenRepository extends JpaRepository<UsuarioToken, Long>
{
	Optional<UsuarioToken> findByToken( String token );
}
