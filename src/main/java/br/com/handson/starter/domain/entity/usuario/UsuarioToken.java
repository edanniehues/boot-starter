package br.com.handson.starter.domain.entity.usuario;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import br.com.handson.common.domain.entity.AbstractEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.envers.Audited;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Audited
public class UsuarioToken extends AbstractEntity implements Serializable
{
	@ManyToOne(optional = false)
	private Usuario usuario;

	@Column(nullable = false, updatable = false, unique = true)
	private String token;
}
