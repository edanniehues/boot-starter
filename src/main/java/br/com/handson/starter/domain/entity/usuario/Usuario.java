package br.com.handson.starter.domain.entity.usuario;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import br.com.handson.common.domain.entity.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Audited
public class Usuario extends AbstractEntity implements UserDetails, Serializable
{
	/**
	 *
	 */
	@NotBlank
	@Column(nullable = false)
	private String email;

	/**
	 *
	 */
	@Column(nullable = false)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String senha;

	/**
	 *
	 */
	@Column(nullable = false)
	private String nome;

	/**
	 *
	 */
	@Column(nullable = false)
	private Boolean ativo;

	/**
	 *
	 */
	@Column(nullable = false)
	@Enumerated(EnumType.ORDINAL)
	private PerfilUsuario perfilUsuario;

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return perfilUsuario.getAuthorities();
	}

	@Override
	@Transient
	public String getPassword()
	{
		return senha;
	}

	@Override
	@Transient
	public String getUsername()
	{
		return email;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	@Transient
	public boolean isEnabled()
	{
		return ativo;
	}
}
