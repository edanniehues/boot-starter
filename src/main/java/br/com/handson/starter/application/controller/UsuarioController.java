package br.com.handson.starter.application.controller;

import br.com.handson.starter.domain.entity.usuario.Usuario;
import br.com.handson.starter.domain.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController
{
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping
	public Page<Usuario> listUsuariosByFilters(
			@RequestParam(value = "filters", required = false) String filters,
			Pageable pageable
	)
	{
		return usuarioService.listByFilters( filters, pageable );
	}

	@GetMapping("/{id}")
	public Usuario findById( @PathVariable("id") long id )
	{
		return usuarioService.findById( id );
	}
}
