package br.com.handson.starter.application.controller;

import java.util.Optional;

import br.com.handson.common.application.request.RequestContext;
import br.com.handson.starter.domain.entity.usuario.Usuario;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/teste")
public class TestController
{
	@GetMapping
	public Optional<Usuario> teste()
	{
		return RequestContext.getUsuario();
	}
}
